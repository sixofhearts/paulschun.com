(ns paulschun.styles
  (:require [garden.def :refer [defstyles]]))

(defstyles main
  [:html :body
   {:background-color "#263D5F"
    :color "white"
    :font-family "Lora, Times New Roman, serif"}]

  [:.navbar {:background-color "#1F324F"}]
  [:.footer-psc
   [:a {:color "white"
       :font-weight 600
       :text-decoration "underline #9CF"
        :text-decoration-thickness "0.15em"
        :transition "all 0.2s"}
    [:&:hover {:text-decoration-color "white"
               :text-decoration-thickness "0.175em"
              :color "white"}]
   ]]
  [:.nav-psc :.footer.footer-psc
   {:background-color "#1F324F"
    :color "white"}
   [:.navbar-item {:background-color "#1F324F"
                   :color "white"}
    [:img {:min-height "2.5rem"}]]
   [:&__contact-btn
    {:background-color "#5AD8D8"
     :height "48px"
     :width "48px"
     :border-radius "50%"
     :display "flex"
     :align-items "center"
     :justify-content "center"
     :transition "all 0.2s"}
    [:&:hover {:background-color "#1F324F"}]]
   ]
  [:.hero-psc
   [:&__text {:font-size "1.5em"
              :font-family "Playfair Display, Times New Roman, serif"}]
   [:&__portrait {:border-radius "50%"}]]
  [:.psc-section {:background "linear-gradient(0deg, rgba(0,0,0,0.2) 0%, rgba(0,0,0,0) 100%)"}]
  [:.psc-content {:font-size "1.25em"
                  :line-height "1.75em"}
   [:a {:color "white"
       :font-weight 600
       :text-decoration "underline #9CF"
        :text-decoration-thickness "0.15em"
        :transition "all 0.2s"}
    [:&:hover {:text-decoration-color "white"
               :text-decoration-thickness "0.175em"
              :color "white"}]
   ]
   [:&__title {:font-family "Playfair Display, Times New Roman, serif"
               :font-weight 700
               :font-size "1.5em"}]
   [:&__drawing {:width "480px"
                 :margin "5em 0 3em"}]
   [:p {:margin-top "1em"}]]
  [:.page-header {:display "inline"
                  :font-weight 600}]
  [:.psc-form
   [:.label {:color "white"
             :font-size "inherit"}]
   [:input :textarea {:width "100%"
                      :font-family "Lora, Times New Roman, serif"
                      :font-size "1em"
                      :padding "0.75em"}]]
  [:.psc-send-btn {:display "inline-block"
                   :font-family "Lora, Times New Roman, serif"
                   :padding "1em"
                   :background-color "#5AD8D8"
                   :border 0
                   :border-radius "1em"
                   :color "white"
                   :cursor "pointer"
                   :transition "all 0.2s"
                   :font-size "0.6em"}
   [:&:hover {:background-color "#1F324F"}]]
  [:.psc-navbar-menu {:display "flex"
                      :justify-content "space-between"
                      :width "100%"}]
  )
