(ns paulschun.views.index
  (:require [paulschun.layout :as layout]))

(defn index []
  (layout/base-layout
   {:raw-title "Paul S. Chun"
    :description "The homepage of Paul S. Chun, a Korean-American entrepreneur and technologist in Tokyo and the founder and CEO of Elbower."
    :content
    [:div
     [:div.psc-section
      [:div.container
       [:section.hero.is-fullheight-with-navbar
        [:div.hero-body
         [:div.columns.is-vcentered
          [:div.column.is-two-thirds.hero-psc__text.has-text-centered
           [:h1.page-header "Paul S. Chun"]
           [:span " is a Tokyo-based American technologist and entrepreneur who's currently building modern job boards."]]
          [:div.column.is-one-third
           [:img.hero-psc__portrait
            {:src "/images/paul-pro.jpg"
             :alt "portrait of Paul S. Chun"
             :title "That's me!"}]]]]
        ]]]
     [:div.psc-section.section
      [:section.hero
       [:div.container
        [:div.hero-body
         [:div.psc-content
          [:h3.psc-content__title.has-text-centered "What I Do"]
          [:div.has-text-centered
           #_[:img.psc-content__drawing
            {:src "/images/trigger-drawing.png"
             :alt "[drawing of a cycle between trigger, action and reward]"
             :title "Sound familiar?"}]]
          [:p "Through my company, "
           [:a {:href "https://elbower.com/"
                :target "_blank"}
            "Elbower"]
           ", I provide a flexible and robust job board solution to enable anyone to serve job seekers and employers related to their industry."]
          #_[:p "If you're interested in knowing why, you can learn more about me on my "
           [:a {:href "/about/"} "about me"]
           " page."]]
         ]]
       ]]
     [:div.psc-section.section
      [:section.hero
       [:div.container
        [:div.hero-body
         [:div.psc-content

          [:h3.psc-content__title.has-text-centered "What I Can Do For You"]
          [:div.has-text-centered
           [:img.psc-content__drawing
            {:src "/images/what-i-can-do-drawing.png"
             :alt "[drawing of the Elbower logo and a seedling in a pot]"
             :title "Grow, baby, grow!"}]]
          [:p "For anyone looking for a modern job board solution, I can refer you to "
           [:a {:href "https://elbower.com/"
                :target "_blank"}
            "Elbower"]
           ", my company, which works on exactly that."]
          [:p "Aside from that, I also take great pleasure in helping young startups navigate the tricky but rewarding road ahead of them as an advisor or a consultant."]
          [:p "Sometimes, I'll also invest money in these companies."]
          [:p "I offer knowledge gained from a broad range of experiences from companies I've founded and companies others have founded in which I've worked. Understanding both business development and software engineering and architecture, I like to think that I can contribute to most young companies as long they're open to it."]
          [:p "If you think you might agree and want to talk, please get in touch through my "
           [:a {:href "/contact/"}
            "contact"]
           " page or on "
           [:a {:href "https://mastodon.social/@psc"
                :rel "me"}
            "Mastodon"]
           "."]]]]]]]

    }))
