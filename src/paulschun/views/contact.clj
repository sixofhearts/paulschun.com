(ns paulschun.views.contact
  (:require [paulschun.layout :as layout]))

(defn contact []
  (layout/base-layout
   {:title "Contact"
    :description "Where and how to get in touch with Paul S. Chun."
    :content
    [:div.container
     [:section.section.hero.psc-content
      [:form.psc-form
       {:method "POST" :action "https://formspree.io/mpzozndo"}
       [:h3.psc-content__title "Contact Paul S. Chun"]
       [:p "If you'd like to contact me for professional reasons or even if you just want to say hi, please use the following form to get in touch."]
       [:h3.psc-content__title "&nbsp;"]
       [:div.columns
        [:div.column
         [:div.field
          [:label.label
           "Message"
           [:div.control
            [:textarea {:name "message"
                        :placeholder "Your message here..."
                        :rows 20}]]]]

         ]

        [:div.column
         [:div.field
          [:label.label "Full Name"
           [:div.control
            [:input.field {:name "name"
                           :type "text"
                           :placeholder "e.g. Ann Janeer"}]]]]
         [:div.field
          [:label.label
           "Email Address"
           [:div.control
            [:input {:name "_replyto"
                     :type "email"
                     :placeholder "e.g. ann@brilliantidea.com"}]]]]
         [:div.field.has-text-right
          [:button.psc-send-btn {:type "submit"} "Send Message &rarr;"]]
         ]
        ]]
      ]]}))
