(ns paulschun.layout
  (:require
   [hiccup.core :refer [html]]
   [hiccup.page :refer [include-css]]
   [paulschun.favicon :refer [favicon-sizes]]))

(defn- custom-favicon-link [prefix dimension]
  [:link {:rel "icon"
          :type "image/png"
          :href (format "/images/favicon/%s%dx%d.png" prefix dimension dimension)}])

(defn flatten-favicons [{:keys [prefix sizes]}]
  (map #(vector prefix %) sizes))

(defn base-layout [{:keys [content title raw-title description]}]
  (html
   [:html
    [:head
     [:title (or raw-title (str "Paul S. Chun &mdash; " title))]
     [:meta {:name "description" :content description}]
     [:meta {:name "robots" :content "index, follow"}]
     [:meta {:name "viewport" :content "width=device-width, initial-scale=1.0"}]
     [:meta {:name "theme-color" :content "#1F324F"}]
     [:meta {:charset "UTF-8"}]
     [:link {:rel "icon" :href "/favicon.ico" :type "image/vnd.microsoft.icon"}]
     [:link {:href "https://fonts.googleapis.com/css2?family=Lora&family=Playfair+Display:wght@400;600&display=swap"
             :rel "stylesheet"}]
     (include-css "/styles/bulma.min.css")
     (include-css "/styles/all.css")
     (->> favicon-sizes
          (map flatten-favicons)
          (apply concat)
          (map #(apply custom-favicon-link %)))]
    [:body
     [:nav.navbar.nav-psc
      [:div.container
       [:div.psc-navbar-menu
        [:div.navbar-item
         [:a {:href "/"}
          [:img {:src "/images/handwritten-name.png"
                 :alt "Paul S. Chun"
                 :title "That's my name, don't wear it out. Or do if you want. It's up to you."}]]]
        [:div.navbar-item
         [:a.nav-psc__contact-btn
          {:href "/contact/"}
          [:svg
           {:xml:space "preserve",
            :style "enable-background:new 0 0 122.88 103.44; width: 32px;",
            :viewbox "0 0 122.88 103.44",
            :y "0px",
            :x "0px",
            :version "1.1"}
           [:g {:fill "white"
                :stroke "white"}
            [:path
             {:d
              "M69.49,102.77L49.8,84.04l-20.23,18.27c-0.45,0.49-1.09,0.79-1.8,0.79c-1.35,0-2.44-1.09-2.44-2.44V60.77L0.76,37.41 c-0.98-0.93-1.01-2.47-0.09-3.45c0.31-0.33,0.7-0.55,1.11-0.67l0,0l118-33.2c1.3-0.36,2.64,0.39,3.01,1.69 c0.19,0.66,0.08,1.34-0.24,1.89l-49.2,98.42c-0.6,1.2-2.06,1.69-3.26,1.09C69.86,103.07,69.66,102.93,69.49,102.77L69.49,102.77 L69.49,102.77z M46.26,80.68L30.21,65.42v29.76L46.26,80.68L46.26,80.68z M28.15,56.73l76.32-47.26L7.22,36.83L28.15,56.73 L28.15,56.73z M114.43,9.03L31.79,60.19l38.67,36.78L114.43,9.03L114.43,9.03z"}]]]
          ]
         ]]
       ]]
     content
     [:footer.footer.footer-psc
      [:div
       [:div.has-text-centered
        [:a {:href "/"} "Paul S. Chun"]
        " &bullet; "
        #_[:a {:href "/about/"} "About Me"]
        #_" &bullet; "
        [:a {:href "/contact/"} "Contact"]
        " &bullet; "
        [:a {:href "https://elbower.com/" :target "_blank"} "Elbower"]
        ]
       [:p.has-text-centered "Copyright &copy; 2020 Paul S. Chun. All rights reserved."]
       [:p.has-text-centered.is-small "Built with love in Tokyo using the fantastic "
        [:a {:href "https://clojure.org/" :target "_blank" :rel "nofollow"} "Clojure"]
        " programming language. Check the source code for this website "
        [:a {:href "https://gitlab.com/sixofhearts/paulschun.com"
             :rel "nofollow"
             :target "_blank"} "here"]
        "."]]]]]))
