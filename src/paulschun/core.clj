(ns paulschun.core
  (:require [optimus.assets :as assets]
            [optimus.export]
            [optimus.optimizations :as optimizations]
            [optimus.prime :as optimus]
            [optimus.strategies :refer [serve-live-assets]]
            [paulschun.routes :as routes]
            [ring.middleware.content-type :refer [wrap-content-type]]
            [stasis.core :as stasis]))

(def target-dir
  "public")

(defn get-assets []
  (assets/load-assets "public" [#"/styles/.*\.css"
                                "/favicon.ico"
                                #"/images/.*\.(jpg|png|gif|jpeg)"]))

(def app
  (-> (routes/pages)
      (stasis/serve-pages)
      (optimus/wrap get-assets optimizations/all serve-live-assets)
      (wrap-content-type)))

(defn export []
  (let [assets (optimizations/all (get-assets) {})]
    ;; (stasis/empty-directory! target-dir)
    (optimus.export/save-assets assets target-dir)
    (stasis/export-pages (routes/pages) target-dir)))
